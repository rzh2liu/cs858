## Pre-reqs:

- Install gcc + gdb
- Install GEF: https://gef.readthedocs.io/en/master/
- Install reqired python packages via: `pip -r requirements.txt`
- Max stack size may need to be adjusted via `ulimit`

Note: project has only been tested on Ubuntu 20.04LTS and Manjaro 5.15.28 

## Files of interest

- analyze.py: for carrying out a round of simulated memory corruptions
- config.py: for configuring simulation parameters
- plot.py: for analyzing data after a round of simulation has completed
- program.c: toy program used as target for analysis
