#!/bin/bash
# 0 for aslr off, 2 for aslr on
sudo bash -c 'echo '$1' > /proc/sys/kernel/randomize_va_space';
sudo cat /proc/sys/kernel/randomize_va_space;
