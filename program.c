#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <inttypes.h>
#include <string.h>
#include <math.h>
typedef int(*fp)();
int LISTSIZE = 0, ENTRYSIZE = 0, RECUR_DEPTH = 0;
static const char arr0[] = "FFFFFFF";
char arr1[] = "FFFFFFF";
extern void *_end;
// char *probe = NULL;
// char *write_data = NULL;
fp **f_list = NULL;
char **s_list = NULL;

int bar() {
    // printf("bar called!\n");
    return 1;
}

int foo() {
    // printf("foo called\n");
    return -1;
}

fp * alloc_f() {
    fp * f = malloc(sizeof(**f_list));
    if(f == NULL) {
        printf("malloc returned NULL\n");
    } else {
        *f = bar;
    }
    return f;
}

char * alloc_s() {
    char *s = malloc(sizeof(char) * ENTRYSIZE);
    if(s == NULL) {
        printf("malloc returned NULL\n");
    } else {
        memset(s, 'F', (sizeof(char) * ENTRYSIZE)-1);
        s[sizeof(char) * ENTRYSIZE-1] = '\0';
    }
    return s;
}

void load_heap() {
    size_t f_count = 0, s_count = 0, len = LISTSIZE/2;
    f_list = malloc(sizeof(*f_list) * len);
    s_list = malloc(sizeof(char*) *len);
    printf("total func ptrs on heap = %ld bytes\n", len*(sizeof(**f_list)+2)); // + 2 for heap chunk metadata
    printf("total str ptrs on heap = %ld bytes\n", len*(sizeof(char) * ENTRYSIZE+2)); // + 2 for heap chunk metadata
    while (f_count < len  && s_count < len) {
        if (rand() % 2 == 0) {
            f_list[f_count++] = alloc_f();
        } else {
            s_list[s_count++] = alloc_s();
        }
    }
    while (f_count < len) {
        f_list[f_count++] = alloc_f();
    }
    while (s_count < len) {
        s_list[s_count++] = alloc_s();
    }
}

// void corrupt_mem() {
//     // char exec_payload[64];
//     if(probe != NULL) {
//         printf("read value at %p = %x\n", probe, *((int *)probe));
//         if (write_data != NULL) {
//             printf("write %ld-bytes at %p: %s\n", strlen(write_data), probe, write_data);        
//             strcpy(probe, write_data); /* write bytes to probe*/
//         } 
//     } 
// }

void load_stack(unsigned int depth) {
    char *stack_ptr = arr1;
    unsigned long l = 0;
    if(depth >= RECUR_DEPTH) {
        printf("stack_ptr addr: %p\n", &stack_ptr);
        printf("printf addr: %p\n", &printf);
        printf("load_stack addr: %p\n", &load_stack);
        printf("heap addr: %p\n", &_end);
        
        // corrupt_mem(); 
        
        // some more operations
        unsigned int i = 0;
        for (i = 0; i < LISTSIZE/2; i++) {
            (*f_list[i])();
            l += strlen(s_list[i]);            
            memset(s_list[i], 'G', sizeof(char) * ENTRYSIZE);
            free(f_list[i]);
            free(s_list[i]);
        }
        if (f_list != NULL)
            free(f_list);
        if (s_list != NULL)
            free(s_list);
    } else {
        load_stack(depth+1);
    }
    l += strlen(stack_ptr);            
}

int main(int argc, char** argv) {
    if (argc >= 2) {
        LISTSIZE = (int)strtol(argv[1], NULL, 0); // heap length
        printf("heap length = %d\n", LISTSIZE);
    }
    if (argc >= 3) {
        RECUR_DEPTH = (int)strtol(argv[2], NULL, 0); // stack depth
        printf("stack depth = %d\n", RECUR_DEPTH);
    }
    ENTRYSIZE = strlen(arr0) + 1;
    // if(argc >= 2) {
    //     probe = (void *) strtol(argv[1], NULL, 16); 
    //     printf("probe: %p\n", probe);
    //     if (argc >= 3) {
    //         write_data = argv[2];
    //         printf("probe set to: %s\n", write_data);
    //     }
    // }
    srand(0); 
    load_heap();    // load heap
    load_stack(0);  // load stack
    return 0;  
}
