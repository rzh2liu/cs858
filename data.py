
class MemoryProfile():
    def __init__(self, ref_addrs, perm_seg_list, mapped_seg_list,
         aslr_offsets=None, canary=None, canary_addrs=None,):
        self.ref_addrs = ref_addrs
        self.perm_seg_list = perm_seg_list
        self.mapped_seg_list = mapped_seg_list
        self.aslr_offsets = aslr_offsets 
        self.canary = canary
        self.canary_addrs = canary_addrs

    def __str__(self):
        _hex_addrs = {key: hex(self.ref_addrs[key]) for key in self.ref_addrs}
        _hex_offsets = {key: hex(self.aslr_offsets[key]) for key in self.aslr_offsets}
        return '(ref_addrs: {}, aslr_offsets: {})'.format(
            _hex_addrs,
            _hex_offsets,
        )

    def __repr__(self):
        return str(self)

    def get_ref_addr(self, name: str):
        return self.ref_addrs[name] if name in self.ref_addrs else None
    
    def get_aslr_offset(self, name: str):
        return self.aslr_offsets[name] if name in self.aslr_offsets else None

    def _search_segment(self, target_seg_list: list, addr: int, left: int, right: int):
        if left > right:
            return None
        mid = int((left + right) / 2)
        seg = target_seg_list[mid]
        if seg.contains(addr):
            return seg
        elif seg.start_addr > addr:
            return self._search_segment(
                target_seg_list, addr, left, mid-1)
        else:
            return self._search_segment(
                target_seg_list, addr, mid+1, right)

    def _filter_segments(self, target_seg_list: list, _path_substr: str):
        result = list()
        for seg in target_seg_list:
            if _path_substr in seg._path:
                result.append(seg)
        return result
    
    def filter_perm_segment(self, _path_substr: str):
        return self._filter_segments(self.perm_seg_list, _path_substr)
    
    def filter_mapped_segment(self, _path_substr: str):
        result = self._filter_segments(self.mapped_seg_list, _path_substr)
        if 'stack' in _path_substr:
            result += self._filter_segments(self.perm_seg_list, "stack")
        if 'heap' in _path_substr:
            result += self._filter_segments(self.perm_seg_list, "heap")
        return result
    
    def get_perm_segment(self, addr: int):
        """
        Search permissions segments by address
        """
        return self._search_segment(
            self.perm_seg_list, addr, 0, len(self.perm_seg_list)-1)
    
    def get_mapped_segment(self, addr: int):
        """
        Search mapped segments by address
        """
        seg = self._search_segment(
            self.mapped_seg_list, addr, 0, len(self.mapped_seg_list)-1)
        # also check heap + stack
        if seg is None:
            heap = self.filter_perm_segment('heap')[0]
            if heap.contains(addr):
                return heap
            stack = self.filter_perm_segment('stack')[0]
            if stack.contains(addr):
                return stack
        return seg
    
    def _get_closest_segment(self, target_seg_list: list, addr: int):
        closest_seg = None
        for seg in target_seg_list:
            if closest_seg is None:
                closest_seg = seg
            elif closest_seg.dist(addr) > seg.dist(addr):
                closest_seg = seg
        assert closest_seg is not None and len(target_seg_list) > 0
        return closest_seg
    
    def get_closest_mapped_segment(self, addr: int):
        """
        Get closest mapped segments by address
        """
        closest_seg = self.get_mapped_segment(addr)
        if closest_seg:
            return closest_seg
        else:
            return self._get_closest_segment(self.mapped_seg_list, addr)

    def get_closest_perm_segment(self, addr: int):
        """
        Get closest permissions segments by address
        """
        closest_seg = self.get_perm_segment(addr)
        if closest_seg:
            return closest_seg
        else:
            return self._get_closest_segment(self.perm_seg_list, addr)

    def _get_smallest_segment(self, target_seg_list: list):
        smallest = None
        for seg in target_seg_list:
            if smallest is None or seg.size() < smallest.size():
                smallest = seg
        return smallest
    
    def _get_largest_segment(self, target_seg_list: list):
        largest = None
        for seg in target_seg_list:
            if largest is None or seg.size() > largest.size():
                largest = seg
        return largest
    
    def get_smallest_perm_segment(self):
        return self._get_smallest_segment(self.perm_seg_list)

    def get_largest_perm_segment(self):
        return self._get_largest_segment(self.perm_seg_list)

    def get_smallest_mapped_segment(self):
        return self._get_smallest_segment(self.mapped_seg_list)

    def get_largest_mapped_segment(self):
        return self._get_largest_segment(self.mapped_seg_list)
        

class MemorySegment(object):
    def __init__(self, start_addr: int, end_addr: int, _path: str, perms: str=None):
        self.start_addr = start_addr
        self.end_addr = end_addr
        self._path = _path
        self.perms = perms

    def __str__(self):
        return "{}->{} {} {} {}".format(
            hex(self.start_addr), 
            hex(self.end_addr), 
            self.perms, 
            hex(self.size()), 
            self._path)
    
    def __repr__(self):
        return str(self)
    
    def size(self):
        s = self.end_addr - self.start_addr
        assert s >= 0
        return s

    def dist(self, addr: int):
        return min(
            abs(self.end_addr - addr), 
            abs(self.start_addr - addr))
    
    def contains(self, addr: int):
        return addr >= self.start_addr and addr < self.end_addr

    def has_perm(self, perm: str):
        if self.perms:
            return perm in self.perms
        return False
    
    
