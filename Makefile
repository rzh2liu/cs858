CC = gcc

CFLAGS  = -g -Wall -lm -Wl,-z,norelro -fno-stack-protector #-fstack-protector #-O0 -z execstack -no-pie #-fsanitize=address

TARGET = program

all: $(TARGET)

$(TARGET): $(TARGET).c
	$(CC) $(CFLAGS) -o $(TARGET).o $(TARGET).c

clean:
	$(RM) $(TARGET).o
