# config ---------------------
ASLR = False
CORE_PATH = '/var/lib/systemd/coredump/'
BREAKS = [92,]  # break points in target program (should be right before corruption)
RAM = 16 * (10**9) # in bytes (for memory stats calculation only)
HEAP_LENGTH = 0x100000
STACK_DEPTH = 0x20000 
DEFENSE_CONFIG = {
    'nmp':         1,
    'canary':      1,
    'cpi':         1,
    'dpi':         1,
    'relro':       1,
}
DUMMY_ADDR = None
# DUMMY_ADDR = hex(0x555555611678)
GDB_NEW_SESSIONS = 0
GDB_TIMEOUT = 8
ITERATIONS = 10000
REQUIRE_W = 1 # require write permission for corrupt location
OUTPUT_NAME = 'out.log'
TARGET_NAME = 'program'           
TARGET_EXEC_NAME = './{}.o'.format(TARGET_NAME) 
TARGET_REGIONS = [ # range of values to target when generating random addr (unused ATM)
    # aslr
    '0x555555550000',
    '0x7ffffffdd000',  
    # non-aslr
    # '0x7ffff0000000',
    # '0x555555500000',
]
TARGET_SEGS = [
    # (name, gdb reference symbol, adjustments?,)
    # ('heap', '&_end', 0,), # &_end: https://stackoverflow.com/questions/3565232/how-to-programmatically-get-the-address-of-the-heap-on-linux
    ('stack', '$sp', 0,),
    ('libc', '&printf', 0,),
    ('program', '&foo', 0,),
    ('s_list', 's_list', 0,),
    ('f_list', 'f_list', 0,),
]
DATA_VALUES = set([ # values that represent KNOWN targets of data pointers
    '0x0046464646464646', # value of 'FFFFFFF' (vast majority of data in target program have this value)
])
CRASH_VALUES = set([ # values that when overwritten will cause target to crash (only used for prediction)
    '0x0000000000000021' # value of chunk size in heap metadata
])
MAX_ADDR = 0x7ffffffff000
PAYLOAD = None # should be in format  
# PAYLOAD = '\\\\x22\\\\x00\\\\x00\\\\x00\\\\x00\\\\x00\\\\x00\\\\x00'
PAYLOAD_MAX = 0x10
PAYLOAD_MIN = 0x1
# ----------------------------
