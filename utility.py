import uuid
import subprocess
import os
import time
import datetime
import random
import shutil

from datetime import datetime
from os import listdir
from os.path import exists
from os.path import isfile, join
from pprint import pprint
from data import MemoryProfile

from config import RAM
from config import ITERATIONS
from config import DEFENSE_CONFIG
from config import TARGET_NAME
from config import TARGET_EXEC_NAME
from config import CORE_PATH
from config import DUMMY_ADDR
from config import TARGET_REGIONS
from config import GDB_TIMEOUT
from config import PAYLOAD
from config import PAYLOAD_MIN
from config import PAYLOAD_MAX
from config import MAX_ADDR
from config import HEAP_LENGTH
from config import STACK_DEPTH
from config import REQUIRE_W
from config import GDB_NEW_SESSIONS
from config import OUTPUT_NAME


def get_file_name():
    name_args = []
    for name in DEFENSE_CONFIG:
        if DEFENSE_CONFIG[name] == True:
            name_args.append(name[:2])
    if REQUIRE_W == True:
        name_args.append('w')
    if GDB_NEW_SESSIONS == True:
        name_args.append('exec')
    name_args.append(str(ITERATIONS))
    out_split = OUTPUT_NAME.split('.')
    assert len(out_split) == 2
    return '{}-{}.{}'.format(
        out_split[0], '-'.join(name_args), out_split[1])

def archive_file(file_name: str):
    if exists(file_name):
        ts = str(datetime.now()).split('.')[0]
        out_split = file_name.split('.')
        shutil.copyfile(file_name, '{} {}.{}'.format(out_split[0], ts, out_split[1]))

def build_target():
    print('=== build target')
    # determine compilation flags based on defense config
    compile_flags = []
    if DEFENSE_CONFIG:
        if DEFENSE_CONFIG['canary'] == False:
            compile_flags.append('-fno-stack-protector')

        if DEFENSE_CONFIG['nmp'] == False:
            compile_flags.append('-z')
            compile_flags.append('execstack')
        
        if DEFENSE_CONFIG['relro']:
            compile_flags.append('-Wl,-z,relro,-z,now')
        else:
            compile_flags.append('-Wl,-z,norelro')

    # remove old binary
    subprocess.call(['rm', '{}.o'.format(TARGET_NAME)])
    # construct build instructions
    build_cmd = ['gcc', '-g']
    build_cmd.extend(compile_flags)
    build_cmd.append('-o')
    build_cmd.append('{}.o'.format(TARGET_NAME))
    build_cmd.append('{}.c'.format(TARGET_NAME))
    print(' '.join(build_cmd))
    # build
    ret = subprocess.call(build_cmd)
    if ret != 0:
        print('build failed with {}'.format(ret))
        exit(ret)

def memory_stats(mp: MemoryProfile):
    alloc_mem = 0
    mapped_mem = 0
    readable_mem = 0
    writable_mem = 0
    executable_mem = 0
    for seg in mp.perm_seg_list:
        alloc_mem += seg.size()
        if seg:
            if seg.has_perm('w'):
                writable_mem += seg.size()
            if seg.has_perm('r'):
                readable_mem += seg.size()
            if seg.has_perm('x'):
                executable_mem += seg.size()
    for seg in mp.mapped_seg_list:
        mapped_mem += seg.size()
    # note: stack & heap don't show up in mapped_seg_list
    stack = mp.filter_perm_segment('stack')[0]
    # writable_mem += stack.size() if stack.has_perm('w') else 0
    # readable_mem += stack.size() if stack.has_perm('r') else 0
    # executable_mem += stack.size() if stack.has_perm('x') else 0
    mapped_mem += stack.size()
    heap = mp.filter_perm_segment('heap')[0]
    # writable_mem += heap.size() if heap.has_perm('w') else 0
    # readable_mem += heap.size() if heap.has_perm('r') else 0
    # executable_mem += heap.size() if heap.has_perm('x') else 0
    mapped_mem += heap.size()

    alloc_p = (alloc_mem/RAM) * 100
    write_p = (writable_mem/alloc_mem) * 100
    read_p = (readable_mem/alloc_mem) * 100
    exec_p = (executable_mem/alloc_mem) * 100
    stack_p = (stack.size()/alloc_mem) * 100
    heap_p = (heap.size()/alloc_mem) * 100
    mapped_p = (mapped_mem/alloc_mem) * 100 
    return alloc_p, write_p, read_p, exec_p, stack_p, heap_p, mapped_p

def get_sample_addr_list(mp: MemoryProfile):
    result = set()
    for seg in mp.mapped_seg_list:
        if REQUIRE_W and seg.has_perm('w') == False:
            continue
        for i in range(seg.start_addr, seg.end_addr):
            result.add(i)
    for seg in mp.perm_seg_list:
        if REQUIRE_W and seg.has_perm('w') == False:
            continue
        for i in range(seg.start_addr, seg.end_addr):
            result.add(i) 
    return list(result)

def random_addr(addr_list: list=None):
    """
    Returns random address in hex str format. 
    Configure config.DUMMY_ADDR to always return same address
    Configure config.TARGET_REGIONS to adjust randomness
    
    :param list addr_list: specify to pick random address from this list
    """
    if DUMMY_ADDR is not None:
        # return hard coded addr from config
        return DUMMY_ADDR
    elif addr_list:
        indx = random.randint(0, len(addr_list)-1)
        return hex(addr_list[indx])

    # By default, the base mapping address is
    # randomly determined by the kernel for each
    # program run. On x86-64, it is typically
    # around 0x5555 5555 0000.
    # addr = hex(uuid.uuid4().int & (1 << 47) - 1 | 0x0000550000000000)
    # addr = uuid.uuid4().int & (1 << 47) - 1 & 0x00007fffffffffff

    range_idx = random.randint(0, len(TARGET_REGIONS)-1)
    prefix = int(TARGET_REGIONS[range_idx], 16)
    prefix_hex = hex(prefix)[2:] # skip '0x'
    i = 0 
    while i < len(prefix_hex) and int(prefix_hex[i], 16) > 0:
        i += 1
    zeros = (len(prefix_hex) - i) * 4 if prefix > 0 else 0
    addr = hex(uuid.uuid4().int & (1 << zeros) - 1 | prefix)
    return (addr)

def get_payload(ptr: str):
    """
    Generate payload
    """
    ptr = int(ptr, 16)
    if PAYLOAD is None:
        length = max(1, random.randint(PAYLOAD_MIN, PAYLOAD_MAX))
        if ptr + length >= MAX_ADDR:
            length = MAX_ADDR - ptr - 1
        payload = []
        for i in range(length):
            b = os.urandom(1)
            while str(b)[3:4] != 'x':
                b = os.urandom(1)
            payload.append('\\{}'.format(str(b)[2:-1]))
        return ''.join(payload)
    return PAYLOAD

def corrupt_memory(gdbmi, addr, payload):   
    i = 0
    addr = int(addr, 16)
    cmds = list()
    while i+8 <= len(payload) and addr < MAX_ADDR:
        write_val = '0x' + ''.join(payload[i:i+8][::-1])
        cmd = 'set {{long}}{}={}'.format(hex(addr), write_val)
        cmds.append(cmd)
        resp = gdbmi.write(cmd)
        filtered = filter_response(resp, _type='notify', _stream='stdout')
        if len(filtered) != 1:
            print('error during corruption!')
            pprint(resp)
            exit(0)
        i += 8
        addr += 8

    while i < len(payload) and addr < MAX_ADDR:
        write_val = '0x{}'.format(payload[i])
        cmd = 'set {{char}}{}={}'.format(hex(addr), write_val)
        cmds.append(cmd)
        resp = gdbmi.write(cmd)
        filtered = filter_response(resp, _type='notify', _stream='stdout')
        if len(filtered) != 1:
            print('error during corruption!')
            pprint(resp)
            exit(0)
        i += 1
        addr += 1
    return cmds
    
    
def _round(i: int, base: int, down:bool = False, up:bool=False):
    m = i % base
    mid = int(base/2)
    if m == 0:
        return i
    elif down:
        return i - m
    elif up:
        return i + (base - m)
    elif m < mid:
        return i - m
    else:
        return i + (base - m)

def parse_args(args: list):
    """
    Return: ptr: int, PAYLOAD: str
    """
    ptr = None
    PAYLOAD = None
    if len(args) > 0:
        ptr = int(args[0], 16)
    if len(args) > 1:
        PAYLOAD = args[1]
    return ptr, PAYLOAD

def filter_response(response, _type='console', _stream='stdout', _contains=None):
    result = []
    for r in response:
        if r['type'] == _type and r['stream'] == _stream:
            result.append(r['payload'])
    return result

def trim_hex(h: str):
    h = h.replace('0x', '')
    i = 0
    while h[i] == '0':
        i+= 1
    return '0x'+h[i:]

def get_memory_value(gdbmi, addr):
    resp = gdbmi.write('x/xg {}'.format(addr), timeout_sec=GDB_TIMEOUT)
    filtered = filter_response(resp, _type='console', _stream='stdout')
    if len(filtered) > 0:
        return filtered[0].replace('\\n', '').split('\\t')[-1]
    return None

def get_memory_values(gdbmi, addr: str, length: int):
    """
    Returns filtered response of memory values covered starting from
    floor(addr, 8) to floor(addr+length, 8) 

    Format: ["<addr>:\t<8 byte value>\t<8 byte value>\n", ...]
    """
    start = int(addr, 16)
    end = start + length
    l_start = _round(start, 8, down=True)
    r_start = _round(start, 8, up=True)
    l_end = _round(end, 8, down=True)
    _size = 0
    if l_start == r_start:
        # start from factor-of-8 address
        _size = max(1,int(length/8))
    else:
        # start from non factor-of-8 address
        _size = 1 + max(0,int((l_end - r_start)/8))
    if l_start < l_end and r_start <= l_end and l_end < end:
        _size += 1
    cmd = 'x/{}xg {}'.format(_size, hex(l_start))
    resp = gdbmi.write(cmd, timeout_sec=GDB_TIMEOUT)
    filtered = filter_response(resp, _type='console', _stream='stdout')
    if len(filtered) > 0:
        return filtered
    else:
        print('error getting memory values')
        pprint(resp)
    return None

def check_crash(response):
    """
    Check response to verify what happened. 
    Return value: (crashed: bool, crash_keyword: str, exec'ed: bool,) 
    """
    filtered = filter_response(response, _type='console', _stream='stdout')
    crash_keywords = ['segmentation fault', 'aborted']
    exec_keywords = ['new program',]
    for payload in filtered:
        _lowered = payload.lower()
        for keyword in crash_keywords:
            if keyword in _lowered:
                return True, keyword, False
        for keyword in exec_keywords:
            if keyword in _lowered:
                return False, None, True
    return False, None, True

def exec_target(args: list):
    """
    Execute target executable. Upon crash, will copy latest
    core dump file into local directory. If no crash occured, 
    None is returned.
    """
    
    cmd = [TARGET_EXEC_NAME] + args
    s = ""
    for c in cmd:
        s += str(c) + " "
    print('=== executing target: {}'.format(s))
    p = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    # print(p.stdout)
    if p.returncode == 0:
        return None
    # find core dumps for target executable
    core_files = [
        '{}{}'.format(CORE_PATH, f) for f in listdir(CORE_PATH) if isfile(join(CORE_PATH, f)) and TARGET_NAME in f]
    # get latest coredump
    create_times = []
    for core_file in core_files:
        t = time.ctime(os.path.getmtime(core_file))
        t = datetime.strptime(t, '%a %b %d %H:%M:%S %Y')
        create_times.append(t)
    core_files = list(zip(create_times, core_files))
    core_files.sort(key=lambda tup: tup[0])
    latest_core = core_files[-1][1]
    local_core = 'core.zst'
    # remove local coredump from previous run
    ret = subprocess.call(['rm', local_core.split('.')[0]],
                          stdout=subprocess.DEVNULL)
    # copy coredump file to local dir
    ret = subprocess.call(['cp', latest_core, local_core],
                          stdout=subprocess.DEVNULL)
    assert ret == 0
    # decompress local copy
    ret = subprocess.call(['unzstd', local_core, '-f'],
                          stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
    assert ret == 0
    # ret = subprocess.call(['rm', local_core], stdout=subprocess.DEVNULL)
    assert ret == 0
    # return name of core dump file (in local directory)
    return local_core.split('.')[0]    