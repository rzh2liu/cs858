import ast
import math
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import numpy as np
import scipy.stats as st

from analyze import gdb_setup
from analyze import gdb_run
from config import OUTPUT_NAME
from config import RAM
from utility import get_file_name
from config import TARGET_REGIONS
from pprint import pprint
from matplotlib import rcParams
rcParams.update({'figure.autolayout': True})
from data import MemorySegment

def parse_log(file_name:str, limit: int=None):
    # note: aslr offsets shift in range 0x10000000000-1
    defense_config = None
    run_data = []
    visited = set()
    mem_stats = None
    dup = 0
    mis_predictions = 0
    i = 0
    with open(file_name, 'r') as f:
        for line in f:
            line = line.replace('\n','')
            if limit and i-5 >= limit:
                break
            if i == 0:
                # line 1: ref_mp
                pass
            elif i == 1:
                # line 2: ref_mp permissions segements
                pass
            elif i == 2:
                # line 3: ref_mp mapped segements
                pass
            elif i == 3:
                # defense config
                defense_config = ast.literal_eval(line.replace('defense:',''))
                remove = []
                for d in defense_config:
                    if defense_config[d] == 0:
                        remove.append(d)
                for d in remove:
                    del defense_config[d]
            elif i == 4:
                # memory stats
                mem_stats = ast.literal_eval(line)
            else:
                # d's contents after d.split(';')
                # d[0] = probe addr in hex     hex str
                # d[1] = overwritten           list of tuples (addr, old val, new val, corrupt val)
                # d[2] = mapped segment        "start->end None hex_size name"
                # d[3] = perm segment          "start->end rwx hex_size name"
                # d[4] = prediction            boolean str (or 'None')
                # d[5] = crashed               boolean str (or 'None')
                # d[6] = bv                    bit vector str  (or 'None')
                d = line.split(';')
                data = dict()
                data['addr'] = d[0]
                if data['addr'] in visited:
                    dup += 1
                else:
                    visited.add(data['addr'])
                data['overwritten'] = ast.literal_eval(d[1])
                # if d[2] == 'None':
                #     data['mapped'] = None
                # else:  
                #     # data[2] {start}->{end} rwx {size(hex)} {name}
                #     sp = d[2].split(' ')
                #     start = int(sp[0].split('->')[0], 16)
                #     end = int(sp[0].split('->')[1], 16)
                #     perms = sp[1]
                #     _path = sp[-1]
                #     data['mapped'] = MemorySegment(
                #         start_addr=start,
                #         end_addr=end,
                #         _path=_path,
                #         perms=perms
                #     )
                data['predicted'] = d[4] == 'True' if d[4] != 'None' else None
                data['crashed'] = d[5] == 'True' if d[5] != 'None' else None
                data['bv'] = d[6] if d[6] != 'None' else None
                if data['bv'] is not None:
                    run_data.append(data)
                    if data['crashed'] == True and data['predicted'] == False:
                        pprint(line)
                        mis_predictions += 1
            i += 1
    print('duplicate addrs: {}'.format(dup))
    print('mis-predictions: {}'.format(mis_predictions))
    return defense_config, run_data, mem_stats


def plot_hist(data: list, title: str, 
        xlabel: str, ylabel: str,
        figsize=(16,6), hex_ticks=True, bins=None):
    if len(data) == 0:
        return
    fig = plt.figure(figsize=figsize)
    ax = fig.gca()
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    if bins:
        plt.hist(data,bins=bins)
    else:
        plt.hist(data)
    plt.title(title)
    if hex_ticks:
        xlabels = map(lambda t: '0x%08X' % int(t), ax.get_xticks())    
        ax.set_xticklabels(xlabels)

def plot_bar(data: list, labels: list, title: str,
            xlabel: str, ylabel: str, 
            figsize=(16,6), log_scale=True):
    if len(data) == 0:
        return
    fig = plt.figure(figsize=figsize)
    ax = fig.gca()
    if log_scale:
        ax.set_yscale('log')
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    plt.bar(labels, data)
    for index, value in enumerate(data):
        if type(value) != int:
            plt.text(index,value, '{:.3}'.format(value))
        else:
            plt.text(index,value, '{}'.format(value))
    ax.set_title(title)

def plot_bar_d(d: dict(), title: str,
        xlabel: str, ylabel: str, fig_name: str,
        figsize=(16,6), log_scale=True):
    kv = []
    data = []
    labels = []
    for k in d:
        kv.append((k,d[k]))
    kv.sort(key=lambda x: x[1])
    labels = [t[0] for t in kv]
    data = [t[1] for t in kv]
    
    plot_bar(
        data=data, 
        labels=labels, 
        xlabel=xlabel,
        ylabel=ylabel,
        title=title,
        figsize=figsize,
        log_scale=log_scale)
    plt.savefig('{}.png'.format(fig_name))

def get_addr_to_num_triggered(run_data: list, only_crashed=True, ignore_bvs=None):
    """
    Returns mapping in format: { addr : avg # bits set }
    """
    result = dict()
    freq = dict()
    for d in run_data:
        if only_crashed and d['crashed'] == False:
            continue
        elif ignore_bv(d['bv'], ignore_bvs):
            continue
        addr = d['addr']
        freq[addr] = freq.get(addr, 0) + 1
        triggered = 0
        for c in d['bv']:
            triggered += 1 if c == '1' else 0
        result[addr] = (result.get(addr, 0) + triggered) / freq[addr]
    return result

def get_combo_triggered_counts(addr_to_bvs: dict, ignore_bvs=None):
    result = dict()
    for addr in addr_to_bvs:
        for bv in addr_to_bvs[addr]:
            if ignore_bv(bv, ignore_bvs):
                continue
            result[bv] = result.get(bv, 0) + addr_to_bvs[addr][bv]
    return result

def get_individual_triggered_counts(addr_to_bvs: dict, defense_config: dict, ignore_bvs=None):
    deployed_defenses = []
    for defense in defense_config:
        if defense_config[defense] == 1:
            deployed_defenses.append(0)
    for addr in addr_to_bvs:
        for bv in addr_to_bvs[addr]:
            assert len(bv) == len(deployed_defenses)
            if ignore_bv(bv, ignore_bvs):
                continue
            for i in range(len(bv)):
                if bv[i] == '1':
                    deployed_defenses[i] += addr_to_bvs[addr][bv]
    result = dict()
    i = 0
    for defense in defense_config:
        result[defense] = deployed_defenses[i]
        i += 1
    return result

def get_addr_to_bvs(run_data: list, only_crashed=True):
    """
    Returns mapping in format: { addr: { bv: # times found } }
    """
    result = dict()
    for d in run_data:
        if only_crashed and d['crashed'] == False:
            continue
        addr = d['addr']
        bv = d['bv']
        if addr not in result:
            result[addr] = dict()
        result[addr][bv] = result[addr].get(bv, 0) + 1
    return result

def ignore_bv(bv: str, _filter_set: set):
    if _filter_set is None:
        return False
    for f in _filter_set:
        if match_bv(bv, f):
            return True
    return False

def match_bv(bv: str, _filter: str):
    matched = True
    for i in range(len(_filter)):
        if _filter[i] != '*' and _filter[i] != bv[i]:
            matched = False
            break
    return matched

def filter_bvs(combo_triggered_counts: dict, _filter: str):
    """
    Filter bv  based on _filter. Returns dictionary of bv matched
    to count
    e.g. _filter="*1111" will match {01111, 11111 }
    """
    result = dict()
    for bv in combo_triggered_counts:
        assert len(_filter) == len(bv)
        if match_bv(bv, _filter):
            result[bv] = result.get(bv,0) + combo_triggered_counts[bv]
    return result

def filter_addrs(addrs: list, filter_target: int, filter_limit: int):
    """
    Filter addresses based on distance (< filter_limit) from filter_target
    """
    result = list()
    for addr in addrs:
        if abs(addr - filter_target) <= filter_limit:
            result.append(addr)
    return result

def freq_to_proba(d: dict, total_samples: int):
    result = dict()
    for k in d:
        result[k] = d[k]/total_samples
    return result

def calc_bv_entropy(combo_triggered_counts: dict, defense_config: dict):
    """
    Calculate entropy for all bit vectors generated
    """
    total_samples = sum([combo_triggered_counts[k] for k in combo_triggered_counts])
    bv_proba = freq_to_proba(d=combo_triggered_counts, total_samples=total_samples)
    entropy = 0
    for bv in combo_triggered_counts:
        if bv in bv_proba:
            proba = bv_proba[bv]
            entropy += -(proba) * math.log(proba) / math.log(2)
    return entropy, bv_proba


def theoretical_calc(run_data, mem_stats):
    """
    """
    # Note: work in progress....
    probe_range = get_corruption_range(run_data)
    alloc_p = mem_stats[0] / 100
    write_p = mem_stats[1] / 100
    read_p = mem_stats[2] / 100
    exec_p = mem_stats[3] / 100
    stack_p = mem_stats[4] / 100
    heap_p = mem_stats[5] / 100
    mapped_p = mem_stats[6] / 100
    alloc = alloc_p * RAM
    mapped = mapped_p * alloc
    heap_size = heap_p * alloc
    stack_size = stack_p * alloc
    # libc_size = 2126034
    scale = 10**-6 # MB
    print('alloc: {:.3}mb'.format(alloc*scale))
    print('mapped: {:.3}mb'.format(mapped*scale))
    print('avail: {:.3}mb'.format(RAM * scale))
    print('heap: {:.3}mb'.format(heap_size*scale))
    print('stack: {:.3}mb'.format(stack_size*scale))
    # probab mapped
    # mapped / probe_range
    
    # canary


def get_corruption_range(run_data):
    _min = float('inf')
    _max = float('-inf')
    for d in run_data:
        addr = int(d['addr'], 16)
        if addr < _min:
            _min = addr
        if addr > _max:
            _max = addr
    return _max-_min

def corrupt_size_to_bits(run_data):
    result = dict()
    for d in run_data:
        if d['bv'] is None:
            continue
        for addr, old_val, new_val, corrupt_data in d['overwritten']:
            _len = len(corrupt_data)/2
            result.update(_len, result.get(_len, 0) + sum([1 if b == '1' else 0 for b in d['bv']]))
    return result

def corrupt_size_to_defenses(run_data):
    result = dict()
    for d in run_data:
        if d['bv'] is None:
            continue
        for addr, old_val, new_val, corrupt_data in d['overwritten']:
            _len = len(corrupt_data)/2
            result.update(_len, result.get(_len, 0) + sum([1 if b == '1' else 0 for b in d['bv']]))
    return result

def run(file_name, limit, show, ignore_bvs):
    defense_config, run_data, mem_stats = parse_log(file_name, limit=limit)
    defenses_deployed = sum([defense_config[d] for d in defense_config])
    corrupt_range = hex(get_corruption_range(run_data))
    addr_to_bvs = get_addr_to_bvs(run_data)
    ind_triggered_counts = get_individual_triggered_counts(addr_to_bvs, defense_config, ignore_bvs=ignore_bvs)
    ind_proba = freq_to_proba(ind_triggered_counts, len(run_data))
    combo_triggered_counts = get_combo_triggered_counts(addr_to_bvs, ignore_bvs=ignore_bvs)
    bv_entropy, bv_proba = calc_bv_entropy(combo_triggered_counts, defense_config)

    addr_to_trig = get_addr_to_num_triggered(run_data, ignore_bvs=ignore_bvs)
    avg_def_trig_list = np.array([addr_to_trig[addr]/defenses_deployed for addr in addr_to_trig])
    std_def_trig = np.std(avg_def_trig_list)
    avg_def_trig = np.mean(avg_def_trig_list)
    # 99% confidence interval
    ci = st.t.interval(
        alpha=0.99, 
        df=len(avg_def_trig_list)-1, 
        loc=np.mean(avg_def_trig_list), 
        scale=st.sem(avg_def_trig_list)) 
    print('=== {}'.format(file_name))
    print('defense config: {}'.format(defense_config))
    print('defenses deployed: {}'.format(defenses_deployed))
    print('total probe range: {}'.format(corrupt_range))
    print('sample size: {}'.format(len(run_data)))
    print('triggered count (individual): {}'.format(ind_triggered_counts))
    print('triggered count (combination): {}'.format(combo_triggered_counts))
    print('bv entropy: {}'.format(bv_entropy))
    # print('bv proba: {}'.format(bv_proba))
    # print('defense proba: {}'.format(ind_proba))
    print('def. triggered mean: {}'.format(avg_def_trig))
    print('def. triggered std: {}'.format(std_def_trig))
    print('def. triggered conf. intvl.: {}'.format(ci))
    print('===')
    theoretical_calc(run_data, mem_stats) 

    # plots
    addrs = np.array([int(k,16) for k in addr_to_bvs])
    heap_addrs = filter_addrs(addrs, 0x550000000000, 0x10000000000)
    stack_addrs = filter_addrs(addrs, 0x7ff000000000, 0x10000000000)
    # print(len(heap_addrs)+len(stack_addrs)+len(libc_addrs))
    plot_bar_d(
        d=ind_triggered_counts, 
        title='',
        xlabel='Deployed Defenses',
        ylabel='Frequency',
        fig_name='{}-ind-distrib'.format(file_name.split('.log')[0],),
        figsize=(16,8)
    )
    plot_bar_d(
        d=combo_triggered_counts, 
        title='',
        xlabel='Observed BVs',
        ylabel='Frequency',
        fig_name='{}-comb-distrib'.format(file_name.split('.log')[0],),
        figsize=(16,6)
    )
    # plot_hist(
    #     data=(addrs), 
    #     title='Overall random address distrib.',
    #     xlabel='Addresses',
    #     ylabel='Frequency',
    # )
    if show:
        plt.show()


if __name__ == '__main__':
    plt.rcParams.update({'font.size': 22})
    plt.rcParams.update({'font.family':'sans-serif'})
    plt.rcParams.update({'font.sans-serif':'Courier New'})
    limit = 100000
    show = False
    ignore_bvs = set([
        # '00000',
    ])

    file_names = [
        './final/out-nm-ca-cp-dp-re-10000.log',
        './final/out-nm-ca-cp-re-10000.log',
        './final/out-nm-ca-dp-re-10000.log',
        './final/out-nm-cp-dp-re-10000.log',
        './final/out-nm-ca-re-10000.log',
        './final/out-nm-re-10000.log',
        './final/out-nm-ca-cp-dp-re-w-10000.log',
        './final/out-nm-ca-cp-re-w-10000.log',
        './final/out-nm-ca-dp-re-w-10000.log',
        './final/out-nm-cp-dp-re-w-10000.log',
        './final/out-nm-ca-re-w-10000.log',
        './final/out-nm-re-w-10000.log',
        # 'out-nm-cp-dp-re-w-10000.log',
    ]

    for file_name in file_names:
        run(file_name=file_name, limit=limit, show=show, ignore_bvs=ignore_bvs)
    