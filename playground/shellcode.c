#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef void plainsig_t(void);
char *probe = 0;
void (*func_ptr)(void);
char global_data[0x1000];
//compile: gcc -g -z execstack -fno-stack-protector -o testshellcode shellcode.c
int main(int argc, char** argv){
    // /bin/bash
    char shellcode[]="\x48\x31\xd2\x52\x48\xb8\x2f\x62\x69\x6e\x2f\x2f\x73\x68\x50\x48\x89\xe7\x52\x57\x48\x89\xe6\x48\x31\xc0\xb0\x3b\x0f\x05";
    // print Hello
    // char shellcode[]="\xb8\x01\x00\x00\x00\x48\xbe\x48\x65\x6c\x6c\x6f\x00\x00\x00\x56\x48\x89\xe6\xba\x05\x00\x00\x00\x0f\x05\xb8\x3c\x00\x00\x00\x0f\x05\x66\x2e\x0f\x1f\x84\x00\x00\x00\x00\x00\x0f\x1f\x44\x00\x00";
    // simply exit
    // char shellcode[]="\x6a\x3c\x58\x0f\x05";
    char buf[256];
    void* heap = malloc(0x1000);
    char *payload = argv[1];
    size_t size = strlen(payload);
    probe = malloc(0x1000);
    strcpy(buf, payload);
    printf("payload size: %ld\n", size);
    printf("buf size: %ld\n", strlen(buf));
    printf("shellcode size: %ld\n", strlen(shellcode));
    // strcpy(buf, shellcode);
    func_ptr = (void (*)()) buf;
    func_ptr();
    free(heap);
    free(probe);
    
    // ((void (*)()) shellcode)();
    //  ((plainsig_t*)shellcode) ();
    return 0;
}