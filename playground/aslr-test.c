#include <stdio.h>
// #include <stdint.h>
#include <stdlib.h>

void func() {
    printf("func!\n");
}

void func2() {
    printf("func2!\n");
}

int main() {
    void *main_addr = main;
    void *func_addr = func;
    void *func2_addr = func2;
    void *printf_addr = printf;
    void *heap_ptr1 = malloc(10);
    void *heap_ptr2 = malloc(10);
    void *heap_ptr3 = malloc(10);
    printf("main_addr: %p\n", main_addr);
    printf("func_addr: %p\n", func_addr);
    printf("func2_addr: %p\n", func2_addr);
    printf("heap_ptr1: %p\n", heap_ptr1);
    printf("heap_ptr2: %p\n", heap_ptr2);
    printf("heap_ptr3: %p\n", heap_ptr3);
    printf("printf_addr: %p\n", printf_addr);
    // *((char *)printf_addr) = 'c';
    printf("offset (main - func): %ld\n", main_addr - func_addr);
    printf("offset (main - func2): %ld\n", main_addr - func2_addr);

    free(heap_ptr1);
    free(heap_ptr2);
}
