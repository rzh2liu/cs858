import subprocess
import time

from pygdbmi.gdbcontroller import GdbController
from tqdm import tqdm
from pprint import pprint
from os import listdir
from os.path import isfile, join

from data import MemoryProfile
from data import MemorySegment
from utility import archive_file
from utility import build_target
from utility import exec_target
from utility import get_file_name
from utility import _round
from utility import random_addr
from utility import parse_args
from utility import filter_response
from utility import get_memory_value
from utility import get_memory_values
from utility import memory_stats
from utility import get_payload
from utility import get_sample_addr_list
from utility import trim_hex
from utility import check_crash
from utility import corrupt_memory
from config import ASLR
from config import RAM
from config import ITERATIONS
from config import CORE_PATH
from config import BREAKS
from config import TARGET_NAME
from config import TARGET_EXEC_NAME
from config import TARGET_SEGS
from config import DEFENSE_CONFIG
from config import GDB_NEW_SESSIONS
from config import GDB_TIMEOUT
from config import OUTPUT_NAME
from config import MAX_ADDR
from config import DATA_VALUES
from config import HEAP_LENGTH
from config import CRASH_VALUES
from config import STACK_DEPTH
from config import REQUIRE_W

def create_gdbmi(aslr=False, core=None):
    """
    Create gdb instance (geq really) with options
        - aslr: to toggle aslr in geq
        - core: to load core dump file along with executable
    """
    # removed "--nx" option from default command to load geq instead of gdb
    gdbmi = GdbController(command=["gdb", "--quiet", "--interpreter=mi3"])
    # load exec file
    response = gdbmi.write('file {}'.format(TARGET_EXEC_NAME))
    # load core file
    if core is not None:
        response = gdbmi.write('core {}'.format(core))
    # enable aslr
    if aslr == True:
        gdbmi.write('aslr on')
    return gdbmi, response

def gdb_setup(aslr=False, core=None):
    """
    Gather relevant memory information including:
        - targeted memory segment reference addresses
        - memory permissions for each segment 
        - mapped memory segments for process
    """
    # create gdb instance
    if aslr is None:
        aslr = ASLR
    gdbmi, _ = create_gdbmi(aslr=aslr, core=core)
    # setup break points
    for bp in BREAKS:
        gdbmi.write('break {}'.format(bp))
    return gdbmi

def get_ref_addrs(gdbmi):
    result = dict()
    for name, ptr, adjustment in TARGET_SEGS:
        cmd = 'x/xg {}'.format(ptr)
        response = gdbmi.write(cmd, timeout_sec=GDB_TIMEOUT)
        filtered = filter_response(response, _type='console', _stream='stdout')
        if len(filtered) > 0:
            payload = filtered[-1].split(':')
            if ' ' in payload[0]:
                payload = payload[0].split(' ')
            result[name] = int(payload[0], 16)
        else:
            print('error getting ref addrs: {}'.format(cmd))
            pprint(response)
            return None
    return result

def parse_perm_segments(gdbmi):
    """
    Get 'r/w/x' permisions for each memory segments for the process
    """
    response = gdbmi.write('vmmap', timeout_sec=GDB_TIMEOUT)
    # pprint(response)
    filtered = filter_response(response, _type='console', _stream='stdout')
    # pprint(filtered[0])
    filtered = filtered[0]
    # remove extraneous characters
    replace_syms = ['\\e[', '31m', '32m', '35m', '34m', '0m', '24m', '4m']
    for sym in replace_syms:
        filtered = filtered.replace(sym, '')
    filtered = filtered.split('\\n')
    # extract segment list
    seg_list = list()
    curr_val = 0
    for line in filtered:
        if len(line) <= 1:
            continue
        values = line.split(' ')
        if values[0][:2] == '0x':
            start = int(values[0], 16)
            if start >= MAX_ADDR:
                continue
            curr_val = start
            end = int(values[1], 16)
            perms = values[3]
            seg = MemorySegment(
                start_addr=start,
                end_addr=end,
                _path=values[4],
                perms=perms)
            seg_list.append(seg)
    seg_list.sort(key=lambda seg: seg.start_addr)
    return seg_list

def parse_mapped_segments(gdbmi):
    response = gdbmi.write('info files')
    filtered = filter_response(response, _type='console', _stream='stdout')
    seg_list = list()
    for line in filtered:
        # interested line in format: '\\t0x0000555555554318 - 0x0000555555554334 is .interp\\n'
        if ' is ' in line:
            line = line.replace('\\t', '').replace('\\n', '')
            line = line.split(' is')
            addrs = line[0].split(' - ')
            name = line[1][1:]
            start = int(addrs[0], 16)
            if start >= MAX_ADDR:
                continue
            end = int(addrs[1], 16)
            seg = MemorySegment(
                start_addr=start,
                end_addr=end,
                _path=name,
                perms=None,)
            seg_list.append(seg)
    seg_list.sort(key=lambda seg: seg.start_addr)
    return seg_list

def get_canary(gdbmi, find_addrs):
    response = gdbmi.write('canary', timeout_sec=GDB_TIMEOUT)
    filtered = filter_response(response, _type='console', _stream='stdout')
    if len(filtered) <= 0:
        pprint("get_canary error:")
        pprint(response)
        exit(1)
    else:
        filtered = filtered[0]
        # remove extraneous characters
        replace_syms = ['\\e[', '1m', '34m[+]', '0m', '\\n']
        for sym in replace_syms:
            filtered = filtered.replace(sym, '')
        end_addr = filtered.split('is at ')[-1].split(',')[0]
        canary = filtered.split('value is ')[-1]
        end_addr = int(end_addr, 16)
        start_addr = end_addr

    canary_addrs = list()

    if find_addrs == True:
        search_canary = trim_hex(canary)[2:]
        response = gdbmi.write('x/xg $sp')
        filtered = filter_response(response,)
        if len(filtered) > 0:
            payload = filtered[-1].split(':')
            start_addr = int(payload[0], 16)
            l = (int((end_addr - start_addr)/8))
            response = gdbmi.write('x/{}gx {}'.format(l, hex(start_addr)))
            filtered = filter_response(response,)
            for line in filtered:
                sp = line.split(':')
                base_addr = int(sp[0], 16)
                values = sp[1].replace('\\n', '').split('\\t')
                if search_canary in values[0]:
                    canary_addrs.append(base_addr)
                if search_canary in values[-1]:
                    canary_addrs.append(base_addr+8)
        if (len(canary_addrs)) < 6:
            print('no canary addrs found:')
            print('canary: {}'.format(canary))
            pprint(filtered[:10])
            exit(1)
    return canary, canary_addrs

def gdb_run(gdbmi, args: list, init_mp: bool, ref_mp=None, find_canary_addrs=False):
    run_cmd = 'run {}'.format(' '.join(args)) 
    resp = gdbmi.write(run_cmd, timeout_sec=GDB_TIMEOUT)
    time.sleep(2.0) # note: seem to need a wait for 'run' cmd to initialize
    if init_mp:
        # get targetted memory segment reference addresses
        ref_addrs = get_ref_addrs(gdbmi)
        if ref_addrs is None:
            return None
        # get memory segment permissions
        perm_seg_list = parse_perm_segments(gdbmi)
        # get mapped memory segments
        mapped_seg_list = parse_mapped_segments(gdbmi)
        # determine aslr offsets
        aslr_offsets = {k: 0 for k in ref_addrs}
        if ref_mp:
            for name in ref_addrs:
                non_aslr_addr = ref_mp.get_ref_addr(name)
                if non_aslr_addr:
                    aslr_offsets[name] = ref_addrs[name]-non_aslr_addr
        # determine canary values and locations
        canary = None
        canary_addrs = None
        if DEFENSE_CONFIG['canary']:
            canary, canary_addrs = get_canary(gdbmi, find_canary_addrs)

        mp = MemoryProfile(
            ref_addrs=ref_addrs,
            perm_seg_list=perm_seg_list,
            mapped_seg_list=mapped_seg_list,
            aslr_offsets=aslr_offsets,
            canary=canary,
            canary_addrs=canary_addrs)
        return mp
    else:
        return None

def calc_aslr_offset(curr_mp: MemoryProfile, ref_mp: MemoryProfile):
    result = {}
    for name in curr_mp.ref_addrs:
        result[name] = hex(
            curr_mp.get_ref_addr(name)-ref_mp.get_ref_addr(name))
    return result

def get_new_value(i: int, start: int, end: int, payload: list, val: str,):
    """
    Return new value that will overwrite the address 'start' based
    provided parameters, next start, and next payload index

    :param int i: payload index. how far into the payload we are
    :param int start: current address we are at
    :param int end: address where overwrite ends
    :param list payload: the data we are overwriting with (should have len 8)
    :param str val: existing value at location (starts with '0x')(should have len 16)
    """
    n_val = ''
    n_start = _round(start, 8, up=True) 
    p_start = _round(start, 8, down=True)
    if n_start == start:
        n_start += 8
    if i == 0 and start + len(payload) <= n_start:
        if end != n_start:
            # |__xxxx__|
            left = (n_start - end + 1)*2
            right = (n_start - start)*2
            n_val = val[2:left] + ''.join(payload[::-1]) + val[2+right:]
        else:
            # |xxxx____|
            n_val = ''.join(payload[::-1]) + val[2+len(payload)*2:] 
        i += len(payload)
    elif n_start <= end:
        if n_start - start < 8:
            # |xxxx____|
            offset = (n_start - start) 
            n_val = ''.join(payload[i:i+offset][::-1]) + val[2+offset*2:] 
            i += offset
        else:
            # |xxxxxxxx|
            n_val = ''.join(payload[i:i+8][::-1])
            i += 8
    elif n_start > end:
        # |___xxxx|
        offset = (n_start - end)
        n_val = val[2:(offset+1)*2] + ''.join(payload[i:i+(8-offset)][::-1])
        i += 8 - offset
    n_val = '0x'+n_val
    assert len(n_val) == len(val)
    return n_val, n_start, i

def check_corruption(mp: MemoryProfile, ref_name: str, ptr: str, payload_len:int):
    start = mp.get_ref_addr(ref_name)
    end = start + int(HEAP_LENGTH / 2) * 8
    ptr = int(ptr, 16)
    # print('{} in {}->{}'.format(hex(ptr), hex(start), hex(end)))
    return start <= ptr + payload_len and ptr < end

def predict_crash(gdbmi, ptr: str, payload: list, mp: MemoryProfile):
    """
    Pre-corruption analysis. Predict whether or not the corruption will
    cause the program to crash and also calculates corruption data returned as
    a list of tuples
    
    :param GDBMIController gdbmi: gdbmi controller object
    :param str ptr: hex string of address to be corrupted
    :param list payload: list of 2-char strs as corruption value
    :para MemoryProfile mp: current memory profile of the program

    return value format: (bool, list,)
    - ret[0] == True: program may crash (depends on defense config)
    - ret[0] == False: program will definitely not crash
    - ret[0] is None: corruption location is invalid
    - ret[1]: data regarding corruption locations [format: (addr: str, old_val: str, new_val: str, corrupt val: str,)]
    """
    overwritten = list()
    ptr = int(ptr, 16)
    # check if mapped location
    mapped_seg = mp.get_mapped_segment(ptr)
    perm_seg = mp.get_perm_segment(ptr)
    if mapped_seg is None and perm_seg is None:
        return True, overwritten
    predict = False
        
    # canary search value
    search_canary = trim_hex(mp.canary)[2:] if mp.canary else None
    # get list of overwritten bytes in str format
    memory_strs = get_memory_values(gdbmi, hex(ptr), len(payload))
    start = ptr
    end = ptr+len(payload)
    i, total_corrupted = 0, 0
    heap = mp.filter_perm_segment('heap')[0]
    stack = mp.filter_perm_segment('stack')[0]
    for memory_str in memory_strs:
        # memory_str format: "<addr>:\t<8 byte value>\t<8 byte value>\n"
        sp = memory_str.replace('\\n', '').split(':')
        vals = sp[-1][2:].strip()
        curr_addr = sp[0].split(' ')[0].strip()
        for old_val in vals.split('\\t'):
            if len(old_val) == 0:
                # could not access memory specified by ptr -> stop immediately
                return None, overwritten
            old_i = i
            # determine new value that will overwrite current value
            new_val, start, i = get_new_value(
                i=i, start=start, end=end, payload=payload, val=old_val)
            corrupt = ''.join(payload[old_i:i][::-1])
            write_len = len(corrupt) / 2
            total_corrupted += write_len
            # cache current values to be overwritten by corruption
            entry = (curr_addr, old_val, new_val, corrupt)
            # print(entry)
            overwritten.append(entry)
            # new value will be the same as old value at current address
            if new_val == old_val:
                continue
            
            p_seg = mp.get_perm_segment(int(old_val,16))
            m_seg = mp.get_mapped_segment(int(old_val,16))

            if old_val in CRASH_VALUES:
                predict = True
            
            if DEFENSE_CONFIG['nmp']:
                # corruption occur to heap pointers?
                corrupt_code_ptrs = check_corruption(
                    mp=mp, ref_name='f_list', ptr=curr_addr, payload_len=write_len)
                corrupt_data_ptrs = check_corruption(
                    mp=mp, ref_name='s_list', ptr=curr_addr, payload_len=write_len)
                in_heap = heap.contains(int(curr_addr,16))
                in_stack = stack.contains(int(curr_addr,16))
                # print('code ptr: {}'.format(corrupt_code_ptrs))
                # print('data ptr: {}'.format(corrupt_data_ptrs))
                # print('in heap: {}'.format(in_heap))
                # print('in stack: {}'.format(in_stack))
                # check if corrupted a pointer in the heap
                if corrupt_data_ptrs or corrupt_code_ptrs or in_heap or in_stack:
                    new_m_seg = mp.get_perm_segment(int(new_val,16))
                    if m_seg is not None and new_m_seg is not None:
                        new_p_seg = mp.get_perm_segment(int(new_val,16))
                        if p_seg is not None and new_p_seg is not None:
                            if p_seg.perms != new_p_seg.perms:
                                # print('nmp perm mismatch')
                                predict = True
                    elif m_seg is not None and new_m_seg is None:
                        # print('nmp unmapped')
                        predict = True
                    elif m_seg is None and new_m_seg is not None:
                        print('nmp mapped?')
                        
            if DEFENSE_CONFIG['canary']:
                # check if canary is in range of memory to be corrupted
                if search_canary is not None and (search_canary in old_val):
                    # print('c')
                    predict = True

            if DEFENSE_CONFIG['cpi']: 
                # check if value corrupted is part of executable memory (i.e. code pointer)
                if m_seg is not None and p_seg is not None and p_seg.has_perm('x'):
                    # print('cpi')
                    predict = True
            
            if DEFENSE_CONFIG['dpi']:
                # check if:
                # - value corrupted is in set of KNOWN data values
                # - value corrupted is pointer to readable memory location
                if old_val in DATA_VALUES:
                    # print('dpi data')
                    predict = True
                elif m_seg is not None and p_seg is not None:
                    if p_seg.has_perm('r') and not p_seg.has_perm('x'):
                        # print('dpi ptr')
                        predict = True
                    elif p_seg.has_perm('w') and not p_seg.has_perm('x'):
                        # print('dpi ptr')
                        predict = True

            if DEFENSE_CONFIG['relro']:
                corrupt_data_ptrs = check_corruption(
                    mp=mp, ref_name='s_list', ptr=curr_addr, payload_len=write_len)
                if corrupt_data_ptrs:
                    new_m_seg = mp.get_mapped_segment(int(new_val,16))
                    # everything related to .got is read-only
                    if new_m_seg and 'got' in new_m_seg._path:
                        print('relro')
                        predict = True
            curr_addr = hex(int(curr_addr, 16) + 8)
    assert total_corrupted == len(payload) 
    assert i == len(payload)
    return predict, overwritten

def gen_bit_vector(gdbmi, ptr:str, payload: str, mp: MemoryProfile, 
                resp=None, overwritten=None):
    """ 
    Returns the bit vector of triggered defenses
    """
    ptr = int(ptr, 16)
    bit_vector = list()
    mapped_seg = mp.get_mapped_segment(ptr)
    perm_seg = mp.get_perm_segment(ptr)
    if mapped_seg is None and perm_seg is None:
        return ''.join(['0']*len(DEFENSE_CONFIG))
    elif len(overwritten) == 0:
        # no corruption occurred -> no need for bv
        return None
    heap = mp.filter_perm_segment('heap')[0]
    stack = mp.filter_perm_segment('stack')[0]
   
    # nmp
    if DEFENSE_CONFIG['nmp']:
        nmp = '0'
        for addr, old_val, new_val, corrupt in overwritten:
            write_len = len(corrupt) / 2
            corrupt_code_ptrs = check_corruption(
                mp=mp, ref_name='f_list', ptr=addr, payload_len=write_len)
            corrupt_data_ptrs = check_corruption(
                mp=mp, ref_name='s_list', ptr=addr, payload_len=write_len)
            in_heap = heap.contains(int(addr,16))
            in_stack = stack.contains(int(addr,16))
            if corrupt_data_ptrs or corrupt_code_ptrs or in_heap or in_stack:
                p_seg = mp.get_perm_segment(int(old_val,16))
                m_seg = mp.get_mapped_segment(int(old_val,16))
                new_m_seg = mp.get_perm_segment(int(new_val,16))
                new_p_seg = mp.get_perm_segment(int(new_val,16))
                if m_seg is not None and new_m_seg is not None:
                    if p_seg is not None and new_p_seg is not None:
                        if p_seg.perms != new_p_seg.perms:
                            nmp = '1'
                            break
        bit_vector.append(nmp)

    # canary
    if DEFENSE_CONFIG['canary']:
        canary = '0'
        search_canary = trim_hex(mp.canary)[2:] if mp.canary else None
        for addr, old_val, new_val, corrupt in overwritten:
            if search_canary in old_val and old_val != new_val:
                canary = '1'
                break
        bit_vector.append(canary)

    # cpi
    if DEFENSE_CONFIG['cpi']:
        cpi = '0'
        for _, old_val, new_val, corrupt in overwritten:
            addr = int(old_val,16)
            seg = mp.get_perm_segment(addr)
            if seg is not None and seg.has_perm('x'):
                cpi = '1'
                break
        bit_vector.append(cpi)

    # dpi
    if DEFENSE_CONFIG['dpi']:
        dpi = '0'
        for _, old_val, new_val, corrupt in overwritten:
            if old_val in DATA_VALUES:
                dpi = '1'
                break
            m_seg = mp.get_mapped_segment(int(old_val,16))
            p_seg = mp.get_perm_segment(int(old_val,16))
            if m_seg is not None and p_seg is not None:
                if p_seg.has_perm('r') and not p_seg.has_perm('x'):
                    dpi = '1'
                    break
                elif p_seg.has_perm('w') and not p_seg.has_perm('x'):
                    dpi = '1'
                    break
        bit_vector.append(dpi)

    # relro
    if DEFENSE_CONFIG['relro']:
        relro = '0'
        for addr, old_val, new_val, corrupt in overwritten:
            write_len = len(corrupt) / 2
            corrupt_data_ptrs = check_corruption(
                    mp=mp, ref_name='s_list', ptr=addr, payload_len=write_len)
            if corrupt_data_ptrs: 
                new_m_seg = mp.get_mapped_segment(int(new_val,16))
                if new_m_seg and 'got' in new_m_seg._path:
                    relro = '1'
                    break
        bit_vector.append(relro)
    return ''.join(bit_vector)

if __name__ == '__main__':
    # create formatted output file name
    output_name = get_file_name()
    # increase max stack size
    subprocess.run(['./ulimit.sh'],)
    # archive existing file
    archive_file(output_name)
    # build binary based on defense config
    build_target()
    
    with open(output_name, 'w') as f:
        # pre analysis (aslr always off here):
        gdbmi = gdb_setup(aslr=False)
        probe = random_addr()
        args = [hex(HEAP_LENGTH), hex(STACK_DEPTH)]
        ref_mp = gdb_run(gdbmi=gdbmi, args=args, init_mp=True)
        f.write('{}\n'.format(ref_mp))
        f.write('{}\n'.format(ref_mp.perm_seg_list))
        f.write('{}\n'.format(ref_mp.mapped_seg_list))
        gdbmi.exit()
        # calculate memory stats
        alloc_p, write_p, read_p, exec_p, stack_p, heap_p, mapped_p = memory_stats(ref_mp) 
        print('alloc./avail.: {:.3f}%'.format(alloc_p))
        print('mapped/alloc.: {:.3f}%'.format(mapped_p))
        print('readable/alloc.: {:.3f}%'.format(read_p))
        print('writable/alloc.: {:.3f}%'.format(write_p))
        print('executable/alloc.: {:.3f}%'.format(exec_p))
        print('stack/alloc.: {:.3f}%'.format(stack_p))
        print('heap/alloc.: {:.3f}%'.format(heap_p))
        print('defense: {}'.format(DEFENSE_CONFIG))
        print('require write perm: {}'.format(REQUIRE_W))
        print('new gdb sessions: {}'.format(GDB_NEW_SESSIONS))
        f.write('{}\n'.format(DEFENSE_CONFIG))
        ref_mem_stats = [alloc_p, write_p, read_p, exec_p, stack_p, heap_p, mapped_p]
        f.write('{}\n'.format(ref_mem_stats))
        
        visited = set()
        overwritten = None
        mem_stats = None
        addr_list = get_sample_addr_list(ref_mp)
        gdbmi = None
        mp = None 
        crashed = False
        prediction = False
        probe = random_addr(addr_list)
        i = 0
        pbar = tqdm(total=ITERATIONS)
        # try random memory access
        while i < ITERATIONS:
            if i == 0 or GDB_NEW_SESSIONS:    
                if gdbmi:
                    gdbmi.exit()            
                # start new session from beginning
                gdbmi = gdb_setup(aslr=False)
                mp = gdb_run(gdbmi=gdbmi, args=args, init_mp=True,)
                # calculate memory stats (stack %, heap %, read %, etc)
                mem_stats = memory_stats(mp)
                for k in range(len(mem_stats)):
                    assert mem_stats[k] == ref_mem_stats[k]
                
            # generate random address to corrupt 
            probe = random_addr(addr_list)
            # reset flags
            crashed = False if GDB_NEW_SESSIONS else None
            prediction = False
            # debugging
            # pprint(mp.perm_seg_list)
            # pprint(mp.mapped_seg_list)
            # only care about crashes that occur due to corruptions in valid memory
            map_seg = mp.get_mapped_segment(int(probe, 16))
            perm_seg = mp.get_perm_segment(int(probe, 16))
            valid_addr = (map_seg is not None or perm_seg is not None)
            if REQUIRE_W == True:
                valid_addr = (perm_seg is not None and perm_seg.has_perm('w'))
            if valid_addr == True:  
                payload = get_payload(probe).split('\\\\x')[1:]
                # print('probe: {}'.format(probe))
                # print('payload: {}'.format(''.join(payload)))
                # predict if memory access will cause crash
                prediction, overwritten = predict_crash(
                    gdbmi=gdbmi, ptr=probe, payload=payload, mp=mp,)

                if GDB_NEW_SESSIONS and len(overwritten) > 0:
                    # corrupt memory via gdb set command
                    cmds = corrupt_memory(gdbmi=gdbmi, addr=probe, payload=payload)
                    ended = False
                    # go past last break point (as we will start a new session next time)
                    while not crashed and not ended:
                        resp = gdbmi.write('continue', timeout_sec=GDB_TIMEOUT)
                        crashed, keyword, _exec = check_crash(resp)
                        # if crashed:
                        #     pprint(filter_response(resp))
                        for msg in filter_response(resp, _type='log'):
                            if 'not being run' in msg:
                                ended = True
                                break
                # generate bit vector of defense mechanism that are violated                  
                bv = gen_bit_vector(
                    gdbmi=gdbmi, ptr=probe, payload=payload, mp=mp, overwritten=overwritten)
                # print('predicted: {}, crashed: {}, bv: {}'.format(prediction, crashed, bv))
                # save result in output file
                log_data = [
                    probe, overwritten, map_seg, perm_seg, prediction, crashed, bv
                ]
                for j in range(len(log_data)):
                    log_data[j] = str(log_data[j])
                f.write('{}\n'.format(';'.join(log_data))) 
                i += 1
                pbar.update(1)
            else:
                print('probe={}'.format(probe))
                print('map_seg={}'.format(map_seg))
                print('perm_seg={}'.format(perm_seg))
        # cleanup
        if gdbmi is not None:
            gdbmi.exit()
        pbar.close()
